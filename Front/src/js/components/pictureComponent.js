// Import extrenal lib LazyLoad
import LazyLoad from "vanilla-lazyload";

/**
 * Resize the picture
 *
 * @param object pictureItem : Picture DOM Element to resize
 * @param object pictureParentItem : Picture parent DOM Element (Element which we base dimensions)
 *
 */
function resizePicture(pictureItem, pictureParentItem) {
  // Picture id
  let pictureItemId = pictureItem.getAttribute("id");
  // Picture dimensions
  let pictureItemWidth = parseFloat(pictureItem.clientWidth);
  let pictureItemHeight = parseFloat(pictureItem.clientHeight);
  // Picture parent dimensions
  let pictureParentItemWidth = parseFloat(pictureParentItem.clientWidth);
  let pictureParentItemHeight = parseFloat(pictureParentItem.clientHeight);
  // Picture ratio
  let ratio = parseFloat(pictureItemHeight / pictureItemWidth);
  // Re-init picture dimensions
  let initWidth = pictureParentItemWidth + 3;
  let initHeight = initWidth * ratio;
  pictureItem.style.width = initWidth + "px";
  pictureItem.style.height = initHeight + "px";
  pictureItemWidth = initWidth;
  pictureItemHeight = initHeight;
  // If picture height is more little its DOM parent
  if (pictureItemHeight < pictureParentItemHeight) {
    let imgWidth = initWidth;
    let imgHeight = initHeight;
    // Loop until picture height equals its DOM parent
    while (imgHeight < pictureParentItemHeight) {
      // Set picture height
      imgHeight = imgWidth * ratio;
      // Set picture dimensions
      pictureItem.style.width = initWidth + "px";
      pictureItem.style.height = initHeight + "px";
      // Increase picture width
      imgWidth++;
    }
  }
  pictureParentItem.classList.add("resized");
}

/**
 * Load and resize picture - Picture size based on container size
 *
 * @param object pictureItem : Picture DOM Element to resize
 * @param object pictureParentItem : Picture parent DOM Element (Element which we base dimensions)
 *
 */
function loadAndResizePicture(pictureItem, pictureParentItem) {
  if (!pictureItem.classList.contains("loaded")) {
    let pictureItemId = pictureItem.getAttribute("id");
    let lazyLoadInstance = new LazyLoad({
      elements_selector: "#" + pictureItemId,
      callback_loaded: () => {
        pictureItem.classList.add("loaded");
        pictureParentItem.classList.add("lazy-loaded");
        resizePicture(pictureItem, pictureParentItem);
      },
    });
  }
}

/**
 * Load picture
 *
 */
function loadPicture() {
  let imagesToLoad = document.querySelectorAll("img.lazy");
  if (imagesToLoad.length) {
    new LazyLoad({
      elements_selector: "img.lazy",
    });
  }
}

export { loadAndResizePicture, loadPicture };
