/**
 * Replace DOM elements
 */
function replaceItems() {
  // Update vh value for CSS
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", vh + "px");
}

export { replaceItems };
