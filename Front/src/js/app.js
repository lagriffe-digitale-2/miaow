// Import components
import { replaceItems } from "./components/DOMComponent";
import * as pictureComponent from "./components/pictureComponent";

/**
 * On window load
 */
window.addEventListener("load", () => {
  // Replace DOM elements
  replaceItems();
  // Lazy-load images
  pictureComponent.loadPicture();
  // Lazy-load and resize images
  let imagesToResizeAndLoad = document.querySelectorAll(
    ".picture-container.with-lazy.to-resize"
  );
  if (imagesToResizeAndLoad.length) {
    imagesToResizeAndLoad.forEach((elt) => {
      // Lazy and resize image
      pictureComponent.loadAndResizePicture(elt.querySelector("img"), elt);
    });
  }
});

/**
 * On window resize
 */
window.addEventListener("resize", () => {
  // replace DOM elements
  replaceItems();
});
